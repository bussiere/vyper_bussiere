import setuptools

with open("README.md", "r") as fh:
    long_description = fh.read()

setuptools.setup(
    name="vyper_bussiere",
    version="0.0.19",
    author="Bussiere",
    author_email="bussiere@gmail.com",
    description="improving vyper",
    long_description=long_description,
    url="https://gitlab.com/bussiere/vyper_bussiere",
    classifiers=[
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: MIT License",
        "Operating System :: OS Independent",
    ],
    install_requires=['vyper-config'],
    packages=setuptools.find_packages(),
)
