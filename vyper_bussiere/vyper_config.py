from vyper import v
from pathlib import Path
import os

def get_viper_data(key="", file="config", path=os.path.abspath(os.getcwd()), type_file="json"):
    v.add_config_path(path)
    v.set_config_name(file)
    v.set_config_type(type_file)
    # v.add_config_path(path)
    v.read_in_config()
    temp = v.get(key)
    v.automatic_env()
    temp2 = v.get(key)
    if temp2 == "" or temp2 is None:
        return temp
    return temp2
